using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vuforia;

public class GameController : MonoBehaviour
{
    public GameObject[] ARContent;
    public int idContent;
    Transform imageTargetTransform;
    UnityEvent finishLoadScene = new UnityEvent();
    public static GameController instance;
    private void Awake() {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void LoadLevel(int id){
        idContent = id;
        finishLoadScene.RemoveAllListeners();
        finishLoadScene.AddListener(LoadAR);
        StartCoroutine(LoadSync(1));
    }
    void LoadAR(){
        // imageTargetTransform = GameObject.Find("ImageTarget").transform;
        // imageTargetTransform.GetComponent<ImageTargetBehaviour>().ImageTargetType = 
        Instantiate(ARContent[idContent]);
        print("Load AR");
    }
    IEnumerator LoadSync(int index){
        AsyncOperation asyncLoad = Application.LoadLevelAsync(index);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        finishLoadScene.Invoke();
    }
    public void Mainmenu(){
        Application.LoadLevel(0);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
